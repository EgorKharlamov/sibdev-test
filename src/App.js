import React, { Component } from "react";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect
} from "react-router-dom";
import Dashboard from "./components/Dashboard/Dashboard";
import Login from "./components/Login/Login";
import Settings from "./components/Settings/Settings";
import PNF from "./components/PNF";
import { connect } from "react-redux";
import './App.sass'
import ApiWork from "./components/ApiWork/ApiWork";

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/login" component={Login} />
          {this.props.state.login.isLogged === false ? (
            <Redirect to="/login" />
          ) : null}

          <Route exact path="/" component={Dashboard} />
          {this.props.state.users.activeUser.redirect ? (
            <Redirect to="/" />
          ) : null}

          <Route exact path="/settings" component={Settings} />

          <Route exact path='/apiwork' component={ApiWork}/>

          <Route component={PNF} />
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return { state };
};

export default connect(mapStateToProps)(App);
