import { isToken } from "../components/Login/tokenGen";

const initialState = {
  redirect: {},
  login: {
    firstName: "",
    middleName: "",
    lastName: "",
    password: "",
    isLogged: false
  },
  users: {
    usersObj: {},
    newUser: {
      firstName: "",
      middleName: "",
      nb: "",
      lastName: "",
      email: "",
      about: "",
      age: "",
      phoneNumber: "",
      passport: "",
      cardsCount: ''
    },
    editUser: {
      editing: false
    },
    activeUser: {
      redirect: false,
      openApi: {
        apiList: {},
      },
      user: {}
    }
  }
};

if (isToken()) initialState.login.isLogged = true;

export default initialState;
