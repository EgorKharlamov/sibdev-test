import initialState from './initialState'

export default function redirectReducer(state = initialState.login, action) {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        isLogged: !state.isLogged
      }
    default:
      return state
  }
}