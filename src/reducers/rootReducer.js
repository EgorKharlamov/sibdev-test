import { combineReducers } from "redux";
import redirectReducer from './redirectReducer'
import loginReducer from './loginReducer'
import addUserReducer from "./addUsersReducer";

export default combineReducers({
    redirectReducer,
    login: loginReducer,
    users: addUserReducer
})