import initialState from './initialState'

export default function redirectReducer(state = initialState.redirect, action) {
    switch (action.type) {
        case 'REDIRECT_URL':
            return {
                ...state,
                redirect: action.payload
            }
            default:
                return state
    }
}