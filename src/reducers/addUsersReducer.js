import initialState from "./initialState";

export default function addUserReducer(state = initialState.users, action) {
  switch (action.type) {
    case "ADD_USER_NOT_BUG":
      return {
        ...state,
        newUser: { ...state.newUser, nb: action.payload }
      };
    case "ADD_USER_FNAME":
      return {
        ...state,
        newUser: { ...state.newUser, firstName: action.payload }
      };
    case "ADD_USER_MNAME":
      return {
        ...state,
        newUser: { ...state.newUser, middleName: action.payload }
      };
    case "ADD_USER_LNAME":
      return {
        ...state,
        newUser: { ...state.newUser, lastName: action.payload }
      };
    case "ADD_USER_AGE":
      return {
        ...state,
        newUser: { ...state.newUser, age: action.payload }
      };
    case "ADD_USER_EMAIL":
      return {
        ...state,
        newUser: { ...state.newUser, email: action.payload }
      };
    case "ADD_USER_PHONE_NUMBER":
      return {
        ...state,
        newUser: { ...state.newUser, phoneNumber: action.payload }
      };
    case "ADD_USER_PASSPORT":
      return {
        ...state,
        newUser: { ...state.newUser, passport: action.payload }
      };
    case "ADD_USER_ABOUT":
      return {
        ...state,
        newUser: { ...state.newUser, about: action.payload }
      };
    case "ADD_USER":
      return {
        ...state,
        usersObj: {
          ...state.usersObj,
          [action.payload]: { ...state.newUser, id: action.payload }
        }
      };
    case "EDIT_USER":
      return {
        ...state,
        newUser: { ...state.newUser, ...state.usersObj[action.payload] },
        editUser: { editing: true }
      };
    case "ADD_EDITED_USER":
      return {
        ...state,
        usersObj: {
          ...state.usersObj,
          [state.newUser.id]: { ...state.newUser }
        },
        editUser: { editing: false }
      };

    case "SESSION_TO_REDUX":
      return {
        ...state,
        usersObj: { ...action.payload }
      };

    case "ACTIVE_USER":
      return {
        ...state,
        activeUser: {
          ...state.activeUser,
          user: { ...state.usersObj[action.payload] }
        }
      };

    case "REDIRECT_TO_DASH":
      return {
        ...state,
        activeUser: { ...state.activeUser, redirect: action.payload }
      };

    case "ADD_API_CARD":
      return {
        ...state,
        activeUser: {
          ...state.activeUser,
          openApi: { ...state.activeUser.openApi, apiList: action.payload }
        }
      };

    case "API_CARDS_COUNT":
      return {
        ...state,
        newUser: { ...state.newUser, cardsCount: action.payload }
      };

    case "CHOOSE_API":
      return {
        ...state,
        ...state,
        activeUser: {
          ...state.activeUser,
          openApi: { ...state.activeUser.openApi, choosenApi: action.payload }
        }
      };
    default:
      return state;
  }
}
