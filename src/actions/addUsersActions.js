export const ADD_USER_FNAME = val => ({ type: "ADD_USER_FNAME", payload: val });

export const ADD_USER_NOT_BUG = val => ({
  type: "ADD_USER_NOT_BUG",
  payload: val
});

export const ADD_USER_MNAME = val => ({ type: "ADD_USER_MNAME", payload: val });

export const ADD_USER_LNAME = val => ({ type: "ADD_USER_LNAME", payload: val });

export const ADD_USER_AGE = val => ({ type: "ADD_USER_AGE", payload: val });

export const ADD_USER_EMAIL = val => ({ type: "ADD_USER_EMAIL", payload: val });

export const ADD_USER_PHONE_NUMBER = val => ({
  type: "ADD_USER_PHONE_NUMBER",
  payload: val
});

export const ADD_USER_PASSPORT = val => ({
  type: "ADD_USER_PASSPORT",
  payload: val
});

export const ADD_USER_ABOUT = val => ({ type: "ADD_USER_ABOUT", payload: val });

export const ADD_USER = val => ({ type: "ADD_USER", payload: val });

export const EDIT_USER = val => ({ type: "EDIT_USER", payload: val });

export const ADD_EDITED_USER = val => ({
  type: "ADD_EDITED_USER",
  payload: val
});

export const SESSION_TO_REDUX = val => ({
  type: "SESSION_TO_REDUX",
  payload: val
});

export const ACTIVE_USER = val => ({ type: "ACTIVE_USER", payload: val });


export const REDIRECT_TO_DASH = val => ({type: 'REDIRECT_TO_DASH', payload: val})

export const ADD_API_CARD = val => ({type: 'ADD_API_CARD', payload: val})

export const API_CARDS_COUNT = val => ({type: 'API_CARDS_COUNT', payload: val})

export const CHOOSE_API = val => ({type: 'CHOOSE_API', payload: val})