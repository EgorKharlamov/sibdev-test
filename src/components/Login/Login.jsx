import React, { Component } from 'react'
import { connect } from 'react-redux'
import { LOGIN } from '../../actions/loginActions'
import { Link, Redirect } from 'react-router-dom'
import { writeTokenLocalStorage } from './tokenGen'
import s from './Login.module.sass'

class Login extends Component {

  redirect = () => {
    if (this.props.state.login.isLogged === true) {
      return <Redirect to={this.props.state.redirectReducer.redirect || '/'} />
    }
  }

  handlerOnClickLogin = () => {
    this.props.login()
    writeTokenLocalStorage()
  }

  render() {
    return (
      <div className={s.wrapper}>
        {this.redirect()}
        <form className={`${s.loginForm}`}>

          <label className={s.label}><input
            // onChange={e => this.setState({ name: e.target.value })}
            className={s.input}
            type="text"
            placeholder='Name' /></label>

          <label className={s.label}><input
            // onChange={e => this.setState({ login: e.target.value })}
            className={s.input}
            type="text"
            placeholder='Login' /></label>

          <label className={s.label}><input
            // onChange={e => this.setState({ password: e.target.value })}
            className={s.input}
            type="password"
            placeholder='Password' /></label>

          <Link to='/' className={s.link}>
            <button
              onClick={(e) => this.handlerOnClickLogin()}
              className={s.button}>login!</button>
          </Link>

        </form>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return ({
    state
  })
}

const mapDispatchToProps = dispatch => ({
  login: () => dispatch(LOGIN())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)