const genToken = () => {
  const TokenGenerator = require('token-generator')({
    salt: `dfgdfgdfgdfgaaergsdfbkyulkio;ewrfqvr`,
    timestampMap: 'abcdefghij',
  });
  return TokenGenerator.generate()
}

export const isToken = () => {
  return localStorage.getItem('token')
}

export const writeTokenLocalStorage = () => {
  const token = genToken()
  localStorage.setItem('token', token)
  return token
}