export default {
  randomCat: {
    name: "Random cat!",
    description: "Do you love kitties? 😻",
    url: "https://aws.random.cat/meow"
  },
  randomDog: {
    name: "Random dog!",
    description: "Who let the dogs out? 🐕",
    url: "https://random.dog/woof.json"
  },
  randomJoke: {
    name: "Random joke!",
    description: "Be careful... Very dark",
    url: "https://sv443.net/jokeapi/category/dark?blacklistFlags=religious"
  },
};
