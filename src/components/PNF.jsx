import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

class PNF extends Component {
  state = {
    redirect: false
  }

  componentDidMount() {
    this.redirectTime = setTimeout(() => this.setState({ redirect: true }), 1000)
  }

  componentWillUnmount() {
    clearTimeout(this.redirectTime)
  }

  render() {
    return (
      this.state.redirect
        ? <Redirect to='/' />
        : <h1><p>PAGE NOT FOUND</p></h1>
    )
  }
}

const mapStateToProps = state => {
  return ({
    state
  })
}

export default connect(
  mapStateToProps
)(PNF)