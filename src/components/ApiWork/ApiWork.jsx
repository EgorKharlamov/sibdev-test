import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import s from "./ApiWork.module.sass";
import { REDIRECT_URL } from "../../actions/redirectActions";
import { Link } from "react-router-dom";

const axios = require("axios");

class ApiWork extends Component {
  state = {
    api: {
      response: ""
    }
  };

  componentDidMount() {
    this.props.redirectUrl(window.location.pathname);

    if (Object.keys(this.props.state.users.activeUser.user).length <= 0)
      window.location.pathname = "/settings";

    axios
      .get(this.props.state.users.activeUser.openApi.choosenApi.url)
      .then(res => {
        this.setState({ api: { ...this.state.api, response: res.data } });
      });
  }

  refreshRequest = async () => {
    await axios
      .get(this.props.state.users.activeUser.openApi.choosenApi.url)
      .then(res => {
        this.setState({ api: { ...this.state.api, response: res.data } });
      });
  };

  renderContent = () => {
    try {
      const { name } = this.props.state.users.activeUser.openApi.choosenApi;
      if (name === "Random dog!") {
        return (
          <Fragment>
            {this.state.api.response.url.slice(
              this.state.api.response.url.length - 3
            ) !== "mp4" ? (
              <img
                src={this.state.api.response.url}
                width="300"
                alt="dog pic"
              />
            ) : (
              <video controls autoPlay width="300">
                <source src={this.state.api.response.url} type="video/mp4" />
              </video>
            )}

            {this.state.api.response.url.slice(
              this.state.api.response.url.length - 4
            ) === "webm" ? (
              <video controls autoPlay width="300">
                <source src={this.state.api.response.url} type="video/webm" />
              </video>
            ) : null}
          </Fragment>
        );
      }
      if (name === "Random cat!") {
        return (
          <Fragment>
            {this.state.api.response.file.slice(
              this.state.api.response.file.length - 3
            ) !== "mp4" ? (
              <img
                src={this.state.api.response.file}
                width="300"
                alt="dog pic"
              />
            ) : (
              <video controls autoPlay width="300">
                <source src={this.state.api.response.file} type="video/mp4" />
              </video>
            )}

            {this.state.api.response.file.slice(
              this.state.api.response.file.length - 4
            ) === "webm" ? (
              <video controls autoPlay width="300">
                <source src={this.state.api.response.file} type="video/webm" />
              </video>
            ) : null}
          </Fragment>
        );
      }
      if (name === "Random joke!") {
        if (this.state.api.response.type === "twopart") {
          return (
            <Fragment>
              <div className={s.setup}>{this.state.api.response.setup}</div>
              <div className={s.delivery}>
                {this.state.api.response.delivery}
              </div>
            </Fragment>
          );
        } else {
          return (
            <Fragment>
              <div className={s.delivery}>{this.state.api.response.joke}</div>
            </Fragment>
          );
        }
      }
    } catch (e) {}
  };

  render() {
    return (
      <Fragment>
        <Link to="/">
          <button>to dash</button>
        </Link>

        <button onClick={() => this.refreshRequest()} type="button">
          refresh
        </button>
        <div className={s.wrapper}>{this.renderContent()}</div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    state
  };
};

const mapDispatchToProps = dispatch => ({
  redirectUrl: val => dispatch(REDIRECT_URL(val))
});

export default connect(mapStateToProps, mapDispatchToProps)(ApiWork);
