import React, { Component } from "react";
import s from "./UserCard.module.sass";
import { connect } from "react-redux";
import {
  EDIT_USER,
  SESSION_TO_REDUX,
  ACTIVE_USER,
  REDIRECT_TO_DASH
} from "../../actions/addUsersActions";

import apiList from "../apiList";
import { ADD_API_CARD } from "../../actions/addUsersActions";

class UserCard extends Component {
  componentDidMount() {
    this.props.sessionToRedux(JSON.parse(sessionStorage.getItem("userList")));
  }

  componentWillUnmount() {
    this.props.redirect(false);
  }

  handlerOnClickEditButton(val) {
    this.props.editUser(val);
  }

  async handlerOnClickChooseActiveUser(val) {
    await this.props.activeUser(val);

    function getRandomElem(obj, amount) {
      if (amount > Object.keys(obj).length) amount = Object.keys(obj).length
      let newObj = {};
      while (Object.keys(newObj).length < amount) {
        const rand = Math.floor(Math.random() * Object.keys(obj).length);
        let breakOpt = false;
        for (let elem in obj) {
          if (obj[elem] === obj[Object.keys(obj)[rand]]) {
            breakOpt = true;
          }
        }
        newObj = {
          ...newObj,
          [Object.keys(obj)[rand]]: { ...obj[Object.keys(obj)[rand]] }
        };
        if (breakOpt) continue;
      }
      return newObj;
    }
    let {cardsCount} = this.props.users.activeUser.user
    this.props.addApiCard(getRandomElem(apiList, cardsCount))

    this.props.redirect(true);
  }

  render() {
    const users = JSON.parse(sessionStorage.getItem("userList"));
    return (
      <div>
        {sessionStorage.getItem("userList")
          ? Object.keys(users).map((item, i) => (
              <div key={i} className={s.userCard}>
                <div
                  onClick={() =>
                    this.handlerOnClickChooseActiveUser(users[item].id)
                  }
                >
                  <div>{users[item].firstName}</div>
                  <div>{users[item].middleName}</div>
                  <div>{users[item].lastName}</div>
                  <div>{users[item].age}</div>
                  <div>{users[item].passport}</div>
                  <div>{users[item].phoneNumber}</div>
                  <div>{users[item].email}</div>
                  <div>{users[item].about}</div>
                </div>
                <div>
                  <button
                    onClick={() =>
                      this.handlerOnClickEditButton(users[item].id)
                    }
                    className={`button ${s.button}`}
                  >
                    edit
                  </button>
                </div>
              </div>
            ))
          : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => ({
  editUser: e => dispatch(EDIT_USER(e)),
  sessionToRedux: e => dispatch(SESSION_TO_REDUX(e)),
  activeUser: e => dispatch(ACTIVE_USER(e)),
  redirect: e => dispatch(REDIRECT_TO_DASH(e)),
  addApiCard: val => dispatch(ADD_API_CARD(val))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserCard);
