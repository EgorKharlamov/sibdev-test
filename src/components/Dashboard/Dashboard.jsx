import React, { Component, Fragment } from "react";
import { REDIRECT_URL } from "../../actions/redirectActions";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import s from "./Dashboard.module.sass";
import { CHOOSE_API } from "../../actions/addUsersActions";

class Dashboard extends Component {
  componentDidMount() {
    this.props.redirectUrl(window.location.pathname);

    if (Object.keys(this.props.state.users.activeUser.user).length === 0) {
      window.location.pathname = "settings";
    }
  }

  handlerOnClickApiCard = (apiList,api) => {
    this.props.chooseApi(apiList[api])
  };

  renderApiCards = () => {
    if (+this.props.state.users.activeUser.user.cardsCount > 0) {
      const { apiList } = this.props.state.users.activeUser.openApi;
      return (
        <div className={s.wrapper}>
          {Object.keys(apiList).map(api => {
            return (
              <Link
                to="/apiwork"
                className={s.cardApi}
                key={api}
                onClick={()=>this.handlerOnClickApiCard(apiList,api)}
              >
                <div className={s.header}>{apiList[api].name}</div>
                <div className={s.description}>{apiList[api].description}</div>
              </Link>
            );
          })}
        </div>
      );
    }
  };

  render() {
    return (
      <Fragment>
        <h1> Dashboard </h1>
        <Link to="/login">
          <button>to login</button>
        </Link>
        <Link to="/settings">
          <button>to settings</button>
        </Link>
        {this.renderApiCards()}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    state
  };
};

const mapDispatchToProps = dispatch => ({
  redirectUrl: val => dispatch(REDIRECT_URL(val)),
  chooseApi: val => dispatch(CHOOSE_API(val))
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
