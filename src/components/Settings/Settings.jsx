import React, { Component, Fragment } from "react";
import { REDIRECT_URL } from "../../actions/redirectActions";
import { connect } from "react-redux";
import {
  ADD_USER_FNAME,
  ADD_USER_MNAME,
  ADD_USER_LNAME,
  ADD_USER_AGE,
  ADD_USER_EMAIL,
  ADD_USER_PHONE_NUMBER,
  ADD_USER_PASSPORT,
  ADD_USER_ABOUT,
  ADD_USER,
  ADD_USER_NOT_BUG,
  ADD_EDITED_USER,
  API_CARDS_COUNT
} from "../../actions/addUsersActions";
import s from "./Settings.module.sass";
import UserCard from "../UserCard/UserCard";

class Settings extends Component {
  componentDidMount() {
    this.props.redirectUrl(window.location.pathname);
  }

  saveToSesStor() {
    let newUserList = {};

    sessionStorage.getItem("userList")
      ? (newUserList = {
          ...JSON.parse(sessionStorage.getItem("userList")),
          ...this.props.state.users.usersObj
        })
      : (newUserList = { ...this.props.state.users.usersObj });

    sessionStorage.setItem("userList", JSON.stringify(newUserList));
    this.props.addUserNb(" ");
  }

  handlerOnClick = async () => {
    this.props.state.users.editUser.editing
      ? await this.props.editUser()
      : await this.props.addUser(new Date().getTime());
    this.saveToSesStor(this.props.state.users.usersObj);

    //clear inputs
    this.props.addUserFName("");
    this.props.addUserMName("");
    this.props.addUserLName("");
    this.props.addUserAge("");
    this.props.addUserEmail("");
    this.props.addUserPhoneNumber("");
    this.props.addUserPassport("");
    this.props.addUserAbout("");
    this.props.apiCardsCount("");
  };

  renderInputs() {
    const {
      addUserFName,
      addUserMName,
      addUserLName,
      addUserAge,
      addUserEmail,
      addUserPhoneNumber,
      addUserPassport,
      addUserAbout,
      apiCardsCount
    } = this.props;

    const changeInput = field => {
      const users = this.props.state.users;
      return users.newUser[field];
    };

    return (
      <div className={s.wrapper}>
        <input
          onChange={e => addUserFName(e.target.value)}
          value={changeInput("firstName")}
          className={`${s.input} ${s.inputImportant}`}
          type="text"
          placeholder="First name"
        />
        <input
          onChange={e => addUserMName(e.target.value)}
          value={changeInput("middleName")}
          className={`${s.input}`}
          type="text"
          placeholder="Middle name"
        />
        <input
          onChange={e => addUserLName(e.target.value)}
          value={changeInput("lastName")}
          className={`${s.input}`}
          type="text"
          placeholder="Last name"
        />
        <input
          onChange={e => addUserAge(e.target.value)}
          value={changeInput("age")}
          className={`${s.input}`}
          type="text"
          placeholder="Age"
        />
        <input
          onChange={e => addUserEmail(e.target.value)}
          value={changeInput("email")}
          className={`${s.input}`}
          type="text"
          placeholder="Email"
        />
        <input
          onChange={e => addUserPhoneNumber(e.target.value)}
          value={changeInput("phoneNumber")}
          className={`${s.input}`}
          type="text"
          placeholder="Phone number"
        />
        <input
          onChange={e => addUserPassport(e.target.value)}
          value={changeInput("passport")}
          className={`${s.input}`}
          type="text"
          placeholder="Passport"
        />
        <input
          onChange={e => apiCardsCount(e.target.value)}
          value={changeInput("cardsCount")}
          className={`${s.input} ${s.inputImportant}`}
          type="text"
          placeholder="api cards (from 1 to infinity)"
        />
        <textarea
          onChange={e => addUserAbout(e.target.value)}
          value={changeInput("about")}
          className={`${s.textarea}`}
          type="text"
          placeholder="About"
        />

        <button
          onClick={() => this.handlerOnClick()}
          className={`button ${s.button}`}
          type="button"
        >
          {!this.props.state.users.editUser.editing ? "Add user" : "Save"}
        </button>
      </div>
    );
  }

  render() {
    return (
      <Fragment>
        {this.renderInputs()}

        {sessionStorage.getItem("userList") ? <UserCard /> : null}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    state
  };
};

const mapDispatchToProps = dispatch => ({
  redirectUrl: val => dispatch(REDIRECT_URL(val)),
  addUserFName: val => dispatch(ADD_USER_FNAME(val)),
  addUserNb: val => dispatch(ADD_USER_NOT_BUG(val)),
  addUserMName: val => dispatch(ADD_USER_MNAME(val)),
  addUserLName: val => dispatch(ADD_USER_LNAME(val)),
  addUserAge: val => dispatch(ADD_USER_AGE(val)),
  addUserEmail: val => dispatch(ADD_USER_EMAIL(val)),
  addUserPhoneNumber: val => dispatch(ADD_USER_PHONE_NUMBER(val)),
  addUserPassport: val => dispatch(ADD_USER_PASSPORT(val)),
  addUserAbout: val => dispatch(ADD_USER_ABOUT(val)),
  addUser: val => dispatch(ADD_USER(val)),
  editUser: val => dispatch(ADD_EDITED_USER(val)),
  apiCardsCount: val => dispatch(API_CARDS_COUNT(val))
});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
